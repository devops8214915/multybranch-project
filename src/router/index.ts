import { createRouter, createWebHistory } from 'vue-router'
import Solution from '../domains/SolutionPage/SolutionView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Solution',
      component: Solution
    },
    {
      path: '/project',
      name: 'Project',
      component: () => import('../domains/ProjectPage/ProjectView.vue')
    },
    {
      path: '/qualification',
      name: 'Qualification',
      component: () => import('../domains/QualificationPage/QualificationView.vue')
    },
    {
      path: '/*',
      name: 'ErrorPage',
      component: Solution
    },
  ]
})

export default router
