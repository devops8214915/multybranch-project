export const QualificationText: string = `
BORISENKO NIKOLAI
Software Senior Engineer with 7 years experience

<strong>Software</strong> - 
not Frontend, not Backend, not Fullstack which solve <i>technical tasks</i>,
but Software Master (whith an emphasis on a Frontend job) who solve <span class='user-for'>bisness and client needs</span> with programming methogic.

Frontend/ Backend/ Fullstack/ Software scale describe the area <u>the Marter’s target</u> within is.


<b>Senior</b> – 
not Junior who write a <i>code segment</i>,
not Middle who write a <i>certain functional</i>,
but Senior Master with <span class='user-for'>and-to-end delivering</span>, inluding developing a general architecture and CI/CD for prodaction deployment.

Therefore can work as a sole Master.
No matter how mach you coded. The point is how mach you delivered.

Junior/ Middle/ Senior scale describe the <u>completeness</u> of Marter’s activity in the area of target.


<b>Engineer</b> – 
not Programmer who work along <i>ruls</i>,
not Developer  who work along a set of certan <i>technology</i>,  
but Engineer who work along <span class='user-for'> princips</span>.

Programmer/  Developer/ Engineer scale describe the type of <u>mental tools</u> the Master use for.


<b>7 years experience</b> -
this scale describe quantitatively, not qualitatively as above, grade of mastery,
and correlated with <u>mastery quantification</u> via terms: <i>beginner, experienced</i> or <i>advanced</i>.

My previos mastery position was Frontend Senior Engineer 
and had had mastery quantification as experienced/advanced.
Currently my master position is <span class='user-for'>Software Senior Engineer</span> with mastery quantification as <span class='user-for'>beginner/ experienced</span>.

`
