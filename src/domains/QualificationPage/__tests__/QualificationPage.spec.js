import { describe, it, expect, beforeEach, afterEach } from 'vitest'
import { mount } from '@vue/test-utils'
import { cleanup } from "@testing-library/vue"
// import {render, screen, fireEvent, cleanup } from "@testing-library/vue"
// import {waitFor} from '@testing-library/dom'

// import router from "../../../router"
import QualificationView from '../QualificationView.vue'

describe('Testing the Vue-components', () => {
  //Arrange
  let wrapper
  beforeEach(() => {
    wrapper = mount(QualificationView)
  })

  afterEach(() => cleanup())


  it('Render Test - QualificationView.vue is rendered properly', async () => {
    //Assert
    expect(wrapper.find("h1").text()).toBe("# Qualification")
  })


  // it.skip('Click & Router Test - Check the Router.push work out when cross-button click', async () => {
  //   //changing the initial value of router.currentRoute._value.path
  //   //which was '/'
  //   await router.push('/farAway')
  //   await fireEvent.click(wrapper.container.querySelector('.header__cross-btn'))

  //   await waitFor(() => {
  //       expect(router.currentRoute._value.path).toBe('/')
  //   })
  // })


  // it.skip('Click & Router Test - Check the Router.push work out when click "BACK to Home" button on', async () => {
  //   const routerPush = vi.spyOn(router, 'push')

  //   await fireEvent.click(screen.getByText('BACK to Home'))

  //   expect(routerPush).toHaveBeenCalled()
  //   expect(routerPush).toHaveBeenCalledWith('/')
  // })
})
