export function clearStringForNumbers(str: any): string {
  if (typeof str === 'string')
    return str.replace(/\D/g, '')
  return ''
}

export function rankingNumbers(str: string): string {
  let valBuffer = str

  //add 0 before dot in the start
  if (valBuffer.startsWith('.'))
    valBuffer = `0${valBuffer}`

  //to eliminate 0 in the start
  else if (valBuffer.startsWith('0') && !valBuffer.startsWith('.', 1) && valBuffer.length > 1)
    valBuffer = valBuffer.substring(1)

  //make a clear number
  const valChunks = valBuffer.split('.')
  valBuffer = `${clearStringForNumbers(valChunks[0])}${valChunks.length > 1 ? '.' : ''}${clearStringForNumbers(valChunks[1]) || ''}`

  return valBuffer
}

