
# # Trial job

````
Trial Project for Nana-Cours, 222

````



The code of the project is based on [GitLab](https://gitlab.com/grategame/nick_trial/-/tree/main). 


After each new commit it run Pipelines up for building the prodaction bunch, executing Authomatic Testing, and 
then implementing a Continuous Delivery for deploying the build on the AWS S3.


Live project is available at [the URL](http://nick-trial.s3-website-us-east-1.amazonaws.com/).


[Click here to see the Video Review]([target_file_name]).



---
<br>
<br>

## ToDo

[Sophie Lewis](mailto:sophie@codewithdragos.com)



---
<br>

## Proposed Solution

*TODO: [ADD_SOLUTION_DESCRIPTION]*
- break down the steps you followed
- use bullet points to explain it in a very structured way



---
## Screenshots




---
## Assumptions
Unit tests are:
* `src/domains/SolutionPage/features/__tests__/utils.spec.ts`
* `src/domains/QualificationPage/__tests__/QualificationPage.spec.js`

E2e tests are:
* `cypress/e2e/menu/Menu.cy.ts`



---
## Libraries / Tools Used
  * JavaScript ES6+
  * TypeScript
  * Vue.js 3
  * Vitest (unit tests)
  * Cypress.io (e2e tests)
  * Git
  * GitLab, Pipeline, CI/CD
  * AWS S3


---
## Setup
`npm install`

`npm run dev`

Open project up with a browser at the port http://localhost:3000.


---
## Running the Unit tests
`npm run test:unit`



---
## Running the E2e tests
`npm run build`

`npm run test:e2e`



---
## Future Work
1. Improve styling. 
2. Improve responsive design.
3. Test for *performance* and implement tactics for increasing it.
4. Complete test coverage to achieve 80% for *unit tests* and 30% for *e2e tests*.
5. Document the API requests with *Swagger*.
6. Deploy on the *AWS EC2*, build the own DB with *AWS RDS*.
7. Leverage CI/CD: implement *Pipeline* with *Automatic Testing* and *Continuous Delivery*.


