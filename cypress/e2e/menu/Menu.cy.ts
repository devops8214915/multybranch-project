// https://docs.cypress.io/api/introduction/api.html

describe('Menu testing', () => {
  it('Routing to AboutPage via click on menu', () => {
    //Arrange
    const AboutURL_RE = new RegExp(`/qualification`)

    //Act
    cy.visit('/')
    cy.get('[href="/qualification"]').click()

    //Assert
    cy.url().should('to.match', AboutURL_RE)
    cy.contains('h1', '# Qualification').should('be.visible')
  })
})
